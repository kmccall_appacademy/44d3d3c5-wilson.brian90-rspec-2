def reverse(string)
  array = string.split.map!{|word| word = word.reverse}
  p array.join
  array.join
end


def reverser(&prc)
  array = prc.call.split
  array.map!{|word| word = word.reverse}
  array.join(" ")
end

def adder(default = 1, &prc)
  prc.call + default
end

def repeater(times=1, &prc)
  times.times {prc.call}
end 

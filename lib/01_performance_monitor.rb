def measure(times=1, &prc)
  if times > 1
    acc = 0
    times.times do
      before = Time.now
      prc.call
      acc += Time.now - before
    end
    return acc / times
  end

  before = Time.now
  prc.call
  Time.now - before
end
